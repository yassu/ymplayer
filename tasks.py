#!/usr/bin/env python
# -*- coding: utf-8 -*-

import term
from invoke import task


def print_title(title: str) -> None:
    term.writeLine("running " + title, term.green)


def _run_commands(ctx, commands):
    for command in commands:
        print(command)
        ctx.run(command)


@task(aliases=['t'])
def test(ctx):
    """ テストする """
    print_title("pytest")
    ctx.run("pytest -vv --cov=ymplayer tests")
    print_title("pytest with cov-report")
    ctx.run("pytest --cov=ymplayer tests --cov-report=html > /dev/null 2>&1")
    print_title("mypy")
    ctx.run("mypy .")
    print_title("flake8")
    ctx.run("flake8 ymplayer/ tests/ *.py")
    print_title("isort")
    ctx.run("isort --check-only ymplayer/ tests/")


# @task(test)
@task
def release(ctx):
    """
    release this project
    """

    _run_commands(ctx, [
        'poetry install',
        'poetry publish --build',
        'git push origin HEAD',
    ])
