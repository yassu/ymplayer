#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from sys import argv, exit, stderr

from ymplayer.player import Player


def main():
    if len(argv) != 2:
        print("use python main.py {sound-filename}", file=stderr)
        exit(1)

    path = Path(argv[1])

    if not path.exists():
        print("use python main.py {sound-filename}", file=stderr)
        exit(1)

    player = Player(str(path))
    player.play()


if __name__ == '__main__':
    main()
