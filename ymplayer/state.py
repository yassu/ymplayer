#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import Enum


class State(Enum):
    READY = 0
    PLAYING = 1
    FINISHED = 2
