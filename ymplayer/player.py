#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from dataclasses import dataclass
from subprocess import DEVNULL, Popen
from subprocess import check_output as subprocess_check_output
from typing import List

from ymplayer.state import State


@dataclass
class Player:
    sound_filename: str
    pid: str = ""
    started: bool = False
    _proc: Popen = Popen("echo", stdout=DEVNULL, stderr=DEVNULL)

    @property
    def play_commands(self) -> List[str]:
        commands = ["mplayer", self.sound_filename]
        return commands

    @property
    def play_command(self) -> str:
        return " ".join(self.play_commands)

    def play(self) -> None:
        self._proc = Popen(self.play_commands, stdout=DEVNULL, stderr=DEVNULL)
        self.started = True
        self.pid = str(self._proc.pid)

    @property
    def state(self) -> State:
        if not self.started:
            return State.READY
        else:
            ps_command = "ps axo pid,command"
            ps_results = subprocess_check_output(
                ps_command, stdin=DEVNULL, stderr=DEVNULL, shell=True).decode().split("\n")
            pat = "^" + self.pid + r"\s+" + self.play_command + "$"
            ms = [re.search(pat, res) for res in ps_results]
            if any(ms):
                return State.PLAYING
            else:
                return State.FINISHED
